** spec **

1. Projeto Java Spring Boot (utilizando módulos necessários):
  - API REST;
  - CRUD de Cliente (id, nome, CPF, dataNascimento); 
  - o CRUD deve possuir na API GET, POST, DELETE, PATCH e PUT.
2. A API de GET deve aceitar "query strings" para pesquisar os clientes por CPF e nome; 
3. É necessário que na API os clientes voltem paginados;
4. O cliente possui a idade calculada considerando a data de nascimento;
5. O banco de dados deve estar em uma imagem Docker ou em um sandbox SAAS (banco SQL);
6. O projeto deve estar em um repositório no BitBucket;
7. Na raiz do projeto deve haver um Postman para apreciação da api.

---

## Opções

1. Maven;
2. Modulos Spring Boot: `RepositoryRestResource`, `AutoConfigureRestDocs`;
3. Testes unitários (API) usando `junit` e `restassured` (restassured.io);
4. A [documentação da aplicação](doc/site/index.html), [testes efetuados](doc/site/surefire-report.html) e serviços encontra-se em `doc/site` e `target/site` (gerado via `mvn site`);
5. Os testes `Postman` são gerados via `Spring Docs` e convertidos para `Postman` via `restdocs-to-postman`;
6. A execução dos testes ocorre via `newman` em um shell script `run-postman-tests.sh`;
7. [A documentação da API](doc/generated-docs/rest-api-cliente.html);
8. O requisito 5 é dispensável pois Java emprega JDBC e só faz sentido ("hoje em dia") num ambiente de CI (não especificado), A configuração pode ser alterado em `application-dev.properties`. Para produção deve ser efetuado através das propriedades `application-prod.properties` e no profile de `prod` do `pom.xml` configurando o driver SGBD de produção.  

Obs: o estágio `mvn site` com "cold-cache" pode levar vários minutos 

---

## Geração dos artefatos

`bash generate-artifact.sh`

ou

`mvn clean site`

---

## Testes Postman

As exceções que ocorrem durante os testes são esperadas por causa da duplicidade de CPF.

`bash run-postman-tests.sh`

![Testes Postman](./doc/postman-pessoa-fisica.gif)

